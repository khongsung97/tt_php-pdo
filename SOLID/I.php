<?php 

/*
 * Interface Segregation Principle
*/

interface AnimalInterface {
	function eat();
	function drink();
	function sleep();
}

interface AniWaterInterface {
	function swim();
}

interface AniSkyInterface {
	function fly();
}

class Human implements AnimalInterface
{
	function eat() {
		return 'ăn cơm';
	}

	function drink() {
		return 'uống ngước sạch';
	}

	function sleep() {
		return 'ngủ trên giường';
	}
}

class Fish implements AnimalInterface, AniWaterInterface {
	function eat() {
		return 'ăn rêu';
	}

	function drink() {
		return 'uống ngước ao';
	}

	function sleep() {
		return 'ngủ dưới nước';
	}

	function swim() {
		return 'bơi được';
	}
}

class Bird implements AniSkyInterface {
	function fly() {
		return 'bay được';
	}
}

echo "<pre>";
$user = new Human();
print_r($user->sleep());

echo '<br>';

$fish = new Fish();
print_r($fish->swim());

 ?>