<?php 
/**
 * Liskov Substitution Principle
 */
class Shape
{
	protected $width;
	protected $height;
	function __construct($width, $height)
	{
		$this->width = $width;
		$this->height = $height;
	}
}

interface ShapeArea {
	public function area();
}

class Rectangle extends Shape implements ShapeArea {
	public function area() {
		return 'Diện tích : ' . $this->width * $this->height;
	}
}

class Square extends Shape implements ShapeArea {
	public function area() {
		return $this->width * $this->width;
	}
}

$rect = new Rectangle(20, 10);
echo $rect->area() . '<br>';

$rect = new Square(20, '');
echo $rect->area();
 ?>