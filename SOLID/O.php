<?php 
/**
 * Open Closed Principle Violation
 */
class Animal
{
	public $leg;
	function __construct($leg)
	{
		$this->leg = $leg;
	}

	function show() {
		return "đi bằng " . $this->leg . " chân";
	}
}

/**
 * extend from class animal
 */
class Human extends Animal
{
	private $name;
	function __construct($name)
	{
		$this->name = $name;
	}

	function future() {
		return $this->name . " " . $this->show();
	}
}

$hum = new Human("con người");
$hum->leg = 2;
echo $hum->future();

 ?>