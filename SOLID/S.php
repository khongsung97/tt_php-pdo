<?php 

/**
 * Single Responsibility Principle Violation
 *
 * class report
 */
class Report
{
	public $title;
	public $content;
	function __construct($title, $content)
	{
		$this->title = $title;
		$this->content = $content;
	}

	public function getReport() {
		return [
			'title' => $this->title,
			'content' => $this->content
		];
	}
}

/*
* interface giao tiếp giữa 2 class
*/
interface ReportFormatInterface {
	public function format(Report $rep);
}

/**
 * class format json
 */
class FormatJson implements ReportFormatInterface
{
	public function format(Report $rep) {
		return json_encode($rep->getReport());
	}
}

$report = new Report('Title', 'this is content');
$report_fomat = new FormatJson();
print_r($report_fomat->format($report));


die();
/**
 * mỗi 1 class chỉ nên có 1 nhiệm vụ: nên tách hàm format sang một class khác
 */
class Report
{
	public $title;
	public $content;
	function __construct($title, $content)
	{
		$this->title = $title;
		$this->content = $content;
	}

	public function getReport() {
		return [
			'title' => $this->title,
			'content' => $this->content
		];
	}

	public function formatJson() {
		return json_encode($this->getReport());
	}
}

$report = new Report('This is Title', 'this is content');
print_r($report->formatJson());
?>