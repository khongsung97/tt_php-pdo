<?php 
	abstract class User {
		private $name;

		public function getName() {
			return $this->name;
		}

		public function setName($name) {
			$this->name = $name;
		}

		abstract public function show();
	}

	/**
	 * 
	 */
	class UserAdmin extends User
	{
		private $role;

		public function setRole($role) {
			$this->role = $role;
		}

		public function getRole() {
			return $this->role;
		}

		function show() {
			echo 'Name: '. $this->getName() . ' <br/> Role: ' . $this->role;
		}
	}

	$sung = new UserAdmin();
	$sung->setName('linh');
	$sung->setRole('admin');
	$sung->show();

 ?>