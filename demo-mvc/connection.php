<?php
	/**
	 * class DB là class singleton
	 * đảm bảo chỉ có duy nhất 1 kết nối tới database 
	 */
	class DB
	{
		private static $conn = NULL;

		public static function getConnect() {
			if(!isset(self::$conn)) {
				try {
					self::$conn = new PDO('mysql:host=localhost;dbname=vcc_demo1_mvc;charset=UTF8','root','12345678');
				} catch (PDOException $e) {
					echo "Lỗi server ";
					die($e->getMessage());
				}
			}
			return self::$conn;
		}

		public static function closeConnect() {
			if(isset(self::$conn)) {
				self::$conn = null;
			}
		}
	}
 ?>