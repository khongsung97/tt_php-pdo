<?php
	try {
		function connectDB() {
			$user = 'root';
			$pass = '12345678';
			$conn = new PDO('mysql:host=localhost;dbname=duan',$user,$pass);
			$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			return $conn;
		}

		function getAll($table) {
			$result = [];
			$conn = connectDB();
			$stmt = $conn->prepare("SELECT * FROM $table");
			$stmt->setFetchMode(PDO::FETCH_OBJ);
			$stmt->execute();
			while ($row = $stmt->fetch()) {
				$result[] = $row;
			}
			return $result;
		}

		function getById($table, $id) {
			$conn = connectDB();
			$stmt = $conn->prepare("SELECT * FROM $table WHERE id = ?");
			$stmt->bindParam(1, $id);
			$stmt->setFetchMode(PDO::FETCH_OBJ);
			$stmt->execute();
			return $stmt->fetch();
		}

		function create($table, $value) {
			$conn = connectDB();
			$stmt = $conn->prepare("INSERT INTO $table VALUES(:id, :name, :email, :password, :status)");
			if($stmt->execute($value)) {
				return true;
			} else {
				return false;
			}

		}

		$data = [
			'id' => '',
			'name' => 'ffffffff',
			'email' => 'anh@gmail.com',
			'password' => '12345678',
			'status' => '0'
		];

		//$ex = $conn->exec("DELETE FROM users WHERE UserName = 'phuong'");
		echo "<pre>";
		$conn = connectDB();
		//create('users', $data);
		//print_r(getById('users', '4'));
		echo str_replace('_', '', ucwords('controller')) . '_controller';
		echo "</pre>";

	} catch (PDOExeption $e) {
		echo $e->getMessage();
	}
	
 ?>